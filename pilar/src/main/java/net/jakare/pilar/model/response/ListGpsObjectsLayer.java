package net.jakare.pilar.model.response;

import com.google.gson.annotations.Expose;

/**
 * Created by Ivan on 19/1/2017.
 */

public class ListGpsObjectsLayer {

    @Expose
    private String id;

    @Expose
    private String nombre;

    @Expose
    private String icono;

    @Expose
    private int estado;

    @Expose
    private Object punto;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getIcono() {
        return icono;
    }

    public void setIcono(String icono) {
        this.icono = icono;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public Object getPunto() {
        return punto;
    }

    public void setPunto(Object punto) {
        this.punto = punto;
    }
}
