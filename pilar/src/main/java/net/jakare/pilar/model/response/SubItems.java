package net.jakare.pilar.model.response;

import com.google.gson.annotations.Expose;

import java.util.List;

/**
 * Created by Ivan on 19/1/2017.
 */

public class SubItems {

    @Expose
    private int id;

    @Expose
    private String nombre;

    @Expose
    private List<Rationing> racionamiento;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public List<Rationing> getRacionamiento() {
        return racionamiento;
    }

    public void setRacionamiento(List<Rationing> racionamiento) {
        this.racionamiento = racionamiento;
    }
}
