package net.jakare.pilar.model.response;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by andresvasquez on 21/12/16.
 */

public class Zone {

    @Expose
    private int id;

    @Expose
    private String nombre;

    @Expose
    private Contour contorno;

    @Expose
    private Object lstObjetosGps;

    @Expose
    private List<SubItems> listaSubItems;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Contour getContorno() {
        return contorno;
    }

    public void setContorno(Contour contorno) {
        this.contorno = contorno;
    }

    public Object getLstObjetosGps() {
        return lstObjetosGps;
    }

    public void setLstObjetosGps(Object lstObjetosGps) {
        this.lstObjetosGps = lstObjetosGps;
    }

    public List<SubItems> getListaSubItems() {
        return listaSubItems;
    }

    public void setListaSubItems(List<SubItems> listaSubItems) {
        this.listaSubItems = listaSubItems;
    }
}
