package net.jakare.pilar.model.response;

import com.google.gson.annotations.Expose;

/**
 * Created by Ivan on 19/1/2017.
 */

public class ContourLayer {

    @Expose
    private String color;

    @Expose
    private Object punto;

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Object getPunto() {
        return punto;
    }

    public void setPunto(Object punto) {
        this.punto = punto;
    }
}
