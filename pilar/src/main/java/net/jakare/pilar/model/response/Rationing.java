package net.jakare.pilar.model.response;

import com.google.gson.annotations.Expose;

/**
 * Created by Ivan on 19/1/2017.
 */

public class Rationing {

    @Expose
    private String fecha;

    @Expose
    private String horaInicio;

    @Expose
    private String horaFin;

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getHoraInicio() {
        return horaInicio;
    }

    public void setHoraInicio(String horaInicio) {
        this.horaInicio = horaInicio;
    }

    public String getHoraFin() {
        return horaFin;
    }

    public void setHoraFin(String horaFin) {
        this.horaFin = horaFin;
    }
}
