package net.jakare.pilar.model.response;

import com.google.gson.annotations.Expose;

/**
 * Created by hola on 11/01/2017.
 */

public class Contour {
    @Expose
    private String tipo;
    @Expose
    private Object capa;

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Object getCapa() {
        return capa;
    }

    public void setCapa(Object capa) {
        this.capa = capa;
    }
}
