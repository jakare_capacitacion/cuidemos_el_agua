package net.jakare.pilar.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Ivan on 19/1/2017.
 */

public class ListGpsObjects {

    @Expose
    private String tipo;

    @SerializedName("capa")
    private Object ListGpsObjectsLayer;

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Object getListGpsObjectsLayer() {
        return ListGpsObjectsLayer;
    }

    public void setListGpsObjectsLayer(Object listGpsObjectsLayer) {
        ListGpsObjectsLayer = listGpsObjectsLayer;
    }
}
