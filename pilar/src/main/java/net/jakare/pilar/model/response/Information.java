package net.jakare.pilar.model.response;

import com.google.gson.annotations.Expose;

/**
 * Created by Ivan on 19/1/2017.
 */

public class Information {

    @Expose
    private String tipo;

    @Expose
    private String valor;

    @Expose
    private String icono;

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public String getIcono() {
        return icono;
    }

    public void setIcono(String icono) {
        this.icono = icono;
    }
}
