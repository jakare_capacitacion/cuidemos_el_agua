package net.jakare.pilar.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by andresvasquez on 21/12/16.
 */

public class SecondLevelResult {

    @Expose
    private List<Zone> zonas;

    @SerializedName("informacion")
    private List<Information> informacion;

    //TODO llenar informacion

    public List<Zone> getZonas() {
        return zonas;
    }

    public void setZonas(List<Zone> zonas) {
        this.zonas = zonas;
    }

    public List<Information> getInformacion() {
        return informacion;
    }

    public void setInformacion(List<Information> informacion) {
        this.informacion = informacion;
    }
}
