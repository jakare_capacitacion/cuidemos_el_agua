package net.jakare.pilar.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by andresvasquez on 21/12/16.
 */

public class GenericServerResult {

    @Expose
    public int intCodigo;

    @SerializedName("resultado")
    public SecondLevelResult resultado;

    public int getIntCodigo() {
        return intCodigo;
    }

    public void setIntCodigo(int intCodigo) {
        this.intCodigo = intCodigo;
    }

    public SecondLevelResult getResultado() {
        return resultado;
    }

    public void setResultado(SecondLevelResult resultado) {
        this.resultado = resultado;
    }
}