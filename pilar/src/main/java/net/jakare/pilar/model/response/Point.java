package net.jakare.pilar.model.response;

import com.google.gson.annotations.Expose;

/**
 * Created by Ivan on 19/1/2017.
 */

public class Point {

    @Expose
    private double lat;

    @Expose
    private double lon;

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }
}
