package net.jakare.pilar.controller.server;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;

import net.jakare.pilar.controller.AppController;
import net.jakare.pilar.model.response.GenericServerResult;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by andresvasquez on 21/12/16.
 */

public class GetData {

    private Response response;

    public GetData(Response response) {
        this.response = response;
    }

    public void execute(String url){
        Map<String, String> jsonParams = new HashMap<String, String>();
        JsonObjectRequest myRequest = new JsonObjectRequest(
                Request.Method.GET,
                url,
                new JSONObject(jsonParams),
                new com.android.volley.Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject objResponse) {
                        String strResult=objResponse.toString();
                        try {
                            GenericServerResult genericServerResult=new Gson().
                                    fromJson(strResult,GenericServerResult.class);
                            response.onSuccess(genericServerResult);
                        }catch (Exception ex){
                            response.onError("Error al parsear");
                        }
                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        response.onError("Error al consumir");
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                return headers;
            }
        };
        AppController.getInstance().addToRequestQueue(myRequest);
    }

    public interface Response{
        void onSuccess(GenericServerResult genericServerResult);
        void onError(String errorMessage);
    }
}
