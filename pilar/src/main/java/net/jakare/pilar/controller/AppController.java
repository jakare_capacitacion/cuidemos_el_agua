package net.jakare.pilar.controller;

import android.app.Application;
import android.content.Context;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.Volley;

/**
 * Created by andresvasquez on 21/12/16.
 */

public class AppController extends Application{

    private static AppController instance;
    public static final String TAG = AppController.class.getSimpleName();

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
    }


    /**
     * Volley
     * */
    private RequestQueue requestQueue;
    public static AppController getApplication(Context context) {
        return (AppController) context.getApplicationContext();
    }

    public synchronized static AppController getInstance(){
        return instance;
    }

    /**
     * *************************************************************
     * Volley
     * *************************************************************
     */
    public RequestQueue getRequestQueue(){
        if(requestQueue==null){
            requestQueue = Volley.newRequestQueue(getApplicationContext());
        }
        return requestQueue;
    }

    public <T> void addToRequestQueue(Request req, String tag){
        req.setTag(TextUtils.isEmpty(tag)?TAG:tag);
        VolleyLog.d("Add request to queue: %s", req.getUrl());



        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (requestQueue != null) {
            requestQueue.cancelAll(tag);
        }
    }

    /**
     * *************************************************************
     *  END Volley
     * *************************************************************
     */
}

