package net.jakare.pilar.controller.database;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import net.jakare.pilar.model.constants.AppConstants;
import net.jakare.pilar.model.response.Information;
import net.jakare.pilar.model.response.Zone;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by andresvasquez on 21/12/16.
 */

public class SharedPreferencesController {

    public final String TAG=SharedPreferencesController.class.getSimpleName();

    private Context mContext;
    private SharedPreferences mPreferences;

    public SharedPreferencesController(Context mContext) {
        this.mContext = mContext;
        this.mPreferences= PreferenceManager.getDefaultSharedPreferences(mContext);
    }

    public void saveDataZone(List<Zone> lstZones){
        SharedPreferences.Editor editor=mPreferences.edit();
        editor.putString(AppConstants.PREFS_DATA,new Gson().toJson(lstZones));
        editor.apply();
    }

    public void saveDataInformation(List<Information> lstInformation){
        SharedPreferences.Editor editor=mPreferences.edit();
        editor.putString(AppConstants.PREFS_DATA,new Gson().toJson(lstInformation));
        editor.apply();
    }

    public List<Zone> getZone(){
        try {
            List<Zone> lstZones=new ArrayList<>();
            String strLstZones=mPreferences.getString(AppConstants.PREFS_DATA,"");
            if(!strLstZones.isEmpty()){
                Type listType = new TypeToken<ArrayList<Zone>>(){}.getType();
                List<Zone> lstZonesGuardadas = new Gson().fromJson(strLstZones, listType);
                lstZones.addAll(lstZonesGuardadas);
                return lstZones;
            }else {
                return new ArrayList<>();
            }
        }catch (Exception ex){
            Log.e(TAG,""+ex.getMessage());
            return new ArrayList<>();
        }
    }

    public List<Information> getInformation(){
        try {
            List<Information> lstInformation=new ArrayList<>();
            String strLstInformation=mPreferences.getString(AppConstants.PREFS_DATA,"");
            if(!strLstInformation.isEmpty()){
                Type listType = new TypeToken<ArrayList<Zone>>(){}.getType();
                List<Information> lstInformationGuardadas = new Gson().fromJson(strLstInformation, listType);
                lstInformation.addAll(lstInformationGuardadas);
                return lstInformation;
            }else {
                return new ArrayList<>();
            }
        }catch (Exception ex){
            Log.e(TAG,""+ex.getMessage());
            return new ArrayList<>();
        }
    }
}
