package net.jakare.jakarecuidemoselagua.model;

/**
 * Created by hola on 03/01/2017.
 */

public class Zone {

    String id;
    String nameZone;

    public Zone(String id, String nameZone) {
        this.id = id;
        this.nameZone = nameZone;
    }

    public String getNameZone() {
        return nameZone;
    }

    public void setNameZone(String nameZone) {
        this.nameZone = nameZone;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
