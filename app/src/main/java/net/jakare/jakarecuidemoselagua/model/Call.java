package net.jakare.jakarecuidemoselagua.model;

/**
 * Created by hola on 03/01/2017.
 */

public class Call {
    String number;
    String type;

    public Call(String type,String number ) {
        this.number = number;
        this.type = type;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
