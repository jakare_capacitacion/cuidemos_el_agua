package net.jakare.jakarecuidemoselagua.model;

/**
 * Created by hola on 03/01/2017.
 */

public class Neighborhood {
    String hour;
    String nameNeighborhood;

    public Neighborhood(String hour, String nameNeighborhood) {
        this.hour = hour;
        this.nameNeighborhood = nameNeighborhood;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public String getNameNeighborhood() {
        return nameNeighborhood;
    }

    public void setNameNeighborhood(String nameNeighborhood) {
        this.nameNeighborhood = nameNeighborhood;
    }
}
