package net.jakare.jakarecuidemoselagua.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.gson.Gson;

import net.jakare.jakarecuidemoselagua.R;
import net.jakare.jakarecuidemoselagua.model.Zone;
import net.jakare.jakarecuidemoselagua.ui.adapter.ZoneAdapter;
import net.jakare.pilar.controller.database.SharedPreferencesController;
import net.jakare.pilar.controller.server.GetData;
import net.jakare.pilar.model.response.GenericServerResult;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hola on 03/01/2017.
 */

public class ItemZoneFragment extends Fragment {

    private RecyclerView recycler;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager lManager;

    private SharedPreferencesController mPrefs;
    private Context mContext;

    private String urlWebService;


    List<Zone> listaZona = new ArrayList<>();

    View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view =  inflater.inflate(R.layout.recycleview, container, false);

        urlWebService = "http://54.159.229.137/desarrollo/public/json/cuidaragua.json";
        mContext = getActivity();
        mPrefs=new SharedPreferencesController(mContext);
        updateFromWs();

        return view;
    }

    private void updateFromWs() {
        updateZone();
    }

    private void updateZone(){
       GetData getData=new GetData(new GetData.Response() {
            @Override
            public void onSuccess(GenericServerResult genericServerResult) {
                List<net.jakare.pilar.model.response.Zone> lstZone=genericServerResult.
                        getResultado().getZonas();
                if(!lstZone.isEmpty()){
                    for (int i=0;i<lstZone.size();i++){
                        String enElTexto=new Gson().toJson(lstZone.get(i).getNombre());
                        listaZona.add(new Zone(new Gson().toJson(lstZone.get(i).getId()).toString(),enElTexto.substring(1,6).toString()));
                        mPrefs.saveDataZone(lstZone);
                    }
                    usarRecycleView();

                }
            }

            @Override
            public void onError(String errorMessage) {
                Toast.makeText(mContext,errorMessage,Toast.LENGTH_SHORT).show();
            }
        });
        getData.execute(urlWebService);
    }

    public void usarRecycleView(){
        recycler = (RecyclerView) view.findViewById(R.id.my_recycler_view);
        recycler.setHasFixedSize(true);

        // Usar un administrador para LinearLayout
        lManager = new LinearLayoutManager(getActivity());
        recycler.setLayoutManager(lManager);

        // Crear un nuevo adaptador
        adapter = new ZoneAdapter(listaZona);
        recycler.setAdapter(adapter);
        recycler.setLayoutManager(new LinearLayoutManager(getContext()));
    }
}
