package net.jakare.jakarecuidemoselagua.ui.activities;

import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;

import net.jakare.jakarecuidemoselagua.R;
import net.jakare.jakarecuidemoselagua.ui.fragments.ItemCallFragment;
import net.jakare.jakarecuidemoselagua.ui.fragments.ItemZoneFragment;
import net.jakare.jakarecuidemoselagua.ui.template.ExpandAndCollapseCardview;

/**
 * Created by hola on 01/01/2017.
 */

public class MenuActivity extends AppCompatActivity {
    private ViewGroup linearLayoutDetails;
    private ViewGroup linearLayoutDetails2;
    private ViewGroup linearLayoutDetails3;
    private ImageView icon;
    private ImageView icon2;
    private ImageView icon3;
    private static final int DURATION = 250;

    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    CollapsingToolbarLayout collapsingToolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        setSupportActionBar((Toolbar) findViewById(R.id.toolbarMenu));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //cargando titulo en el toolbar
        loadTitleActivity();

        //cargando los fragmentos
        loadFragments();


        linearLayoutDetails = (ViewGroup) findViewById(R.id.linearLayoutDetails);
        linearLayoutDetails2 = (ViewGroup) findViewById(R.id.linearLayoutDetails2);
        linearLayoutDetails3 = (ViewGroup) findViewById(R.id.linearLayoutDetails3);
        icon = (ImageView) findViewById(R.id.imageViewExpand);
        icon2 = (ImageView) findViewById(R.id.imageViewExpand2);
        icon3 = (ImageView) findViewById(R.id.imageViewExpand3);

    }
    public void loadTitleActivity(){
        collapsingToolbar=(CollapsingToolbarLayout) findViewById(R.id.collapsingToolbar);
        collapsingToolbar.setTitle("#CuidemosElAgua");
        collapsingToolbar.setExpandedTitleColor(getResources().getColor(R.color.primaryText));
    }
    public void loadFragments(){
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        ItemZoneFragment zona = new ItemZoneFragment();
        fragmentTransaction.replace(R.id.linearLayoutDetails, zona);
        fragmentTransaction.commit();


        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        ItemCallFragment call = new ItemCallFragment();
        fragmentTransaction.replace(R.id.linearLayoutDetails2, call);
        fragmentTransaction.commit();
    }
    public void DetailsRationing(View view) {
        if (linearLayoutDetails.getVisibility() == View.GONE) {
            if(linearLayoutDetails2.getVisibility() != View.GONE){
                ExpandAndCollapseCardview.collapse(linearLayoutDetails2, DURATION);
                icon2.setImageResource(R.drawable.ic_expand_less);
                rotate2(180.0f);
            }
            if(linearLayoutDetails3.getVisibility() != View.GONE){
                ExpandAndCollapseCardview.collapse(linearLayoutDetails3, DURATION);
                icon3.setImageResource(R.drawable.ic_expand_less);
                rotate3(180.0f);
            }
            ExpandAndCollapseCardview.expand(linearLayoutDetails, DURATION);
            icon.setImageResource(R.drawable.ic_expand_more);
            rotate(-180.0f);
        } else {
            ExpandAndCollapseCardview.collapse(linearLayoutDetails, DURATION);
            icon.setImageResource(R.drawable.ic_expand_less);
            rotate(180.0f);
        }
    }
    public void DetailsInformation(View view) {
        if (linearLayoutDetails2.getVisibility() == View.GONE) {
            if(linearLayoutDetails.getVisibility() != View.GONE){
                ExpandAndCollapseCardview.collapse(linearLayoutDetails, DURATION);
                icon.setImageResource(R.drawable.ic_expand_less);
                rotate(180.0f);
            }
            if(linearLayoutDetails3.getVisibility() != View.GONE){
                ExpandAndCollapseCardview.collapse(linearLayoutDetails3, DURATION);
                icon3.setImageResource(R.drawable.ic_expand_less);
                rotate3(180.0f);
            }
            ExpandAndCollapseCardview.expand(linearLayoutDetails2, DURATION);
            icon2.setImageResource(R.drawable.ic_expand_more);
            rotate2(-180.0f);
        } else {
            ExpandAndCollapseCardview.collapse(linearLayoutDetails2, DURATION);
            icon2.setImageResource(R.drawable.ic_expand_less);
            rotate2(180.0f);
        }
    }
    public void DetailsAbout(View view) {
        if (linearLayoutDetails3.getVisibility() == View.GONE) {
            if(linearLayoutDetails.getVisibility() != View.GONE){
                ExpandAndCollapseCardview.collapse(linearLayoutDetails, DURATION);
                icon.setImageResource(R.drawable.ic_expand_less);
                rotate(180.0f);
            }
            if(linearLayoutDetails2.getVisibility() != View.GONE){
                ExpandAndCollapseCardview.collapse(linearLayoutDetails2, DURATION);
                icon2.setImageResource(R.drawable.ic_expand_less);
                rotate2(180.0f);
            }
            ExpandAndCollapseCardview.expand(linearLayoutDetails3, DURATION);
            icon3.setImageResource(R.drawable.ic_expand_more);
            rotate3(-180.0f);
        } else {
            ExpandAndCollapseCardview.collapse(linearLayoutDetails3, DURATION);
            icon3.setImageResource(R.drawable.ic_expand_less);
            rotate3(180.0f);
        }
    }
    private void rotate(float angle) {
        Animation animation = new RotateAnimation(0.0f, angle, Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f);
        animation.setFillAfter(true);
        animation.setDuration(DURATION);
        icon.startAnimation(animation);
    }
    private void rotate2(float angle) {
        Animation animation = new RotateAnimation(0.0f, angle, Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f);
        animation.setFillAfter(true);
        animation.setDuration(DURATION);
        icon2.startAnimation(animation);
    }
    private void rotate3(float angle) {
        Animation animation = new RotateAnimation(0.0f, angle, Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f);
        animation.setFillAfter(true);
        animation.setDuration(DURATION);
        icon3.startAnimation(animation);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
