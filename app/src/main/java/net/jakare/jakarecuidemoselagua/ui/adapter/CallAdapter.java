package net.jakare.jakarecuidemoselagua.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import net.jakare.jakarecuidemoselagua.R;
import net.jakare.jakarecuidemoselagua.model.Call;

import java.util.List;

/**
 * Created by hola on 03/01/2017.
 */

public class CallAdapter extends RecyclerView.Adapter<CallAdapter.CallViewHolder> {
    private static List<Call> items;
    private View v;

    public class CallViewHolder extends RecyclerView.ViewHolder {
        // Campos respectivos de un item
        public TextView number;
        public ImageView image;
        public CallViewHolder(final View v) {
            super(v);
            number = (TextView) v.findViewById(R.id.txtNumber);
            image = (ImageView)v.findViewById(R.id.image);
        }
    }

    public CallAdapter(List<Call> items) {
        this.items = items;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public CallAdapter.CallViewHolder onCreateViewHolder(final ViewGroup viewGroup, final int i) {
        v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.fragment_item_call, viewGroup, false);
        return new CallAdapter.CallViewHolder(v);
    }

    @Override
    public void onBindViewHolder(CallAdapter.CallViewHolder viewHolder, int i) {
        viewHolder.number.setText(items.get(i).getNumber());
        if(items.get(i).getType().equals("whatsapp")){
            viewHolder.image.setImageResource(R.drawable.whatsapp);
        }else if(items.get(i).getType().equals("llamada")){
            viewHolder.image.setImageResource(R.drawable.ic_call);
        }else if (items.get(i).getType().equals("email")){
            viewHolder.image.setImageResource(R.drawable.ic_email);
        }
    }
}