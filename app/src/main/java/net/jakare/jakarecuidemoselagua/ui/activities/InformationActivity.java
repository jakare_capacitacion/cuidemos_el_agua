package net.jakare.jakarecuidemoselagua.ui.activities;


import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.text.format.DateFormat;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import net.jakare.jakarecuidemoselagua.R;
import net.jakare.jakarecuidemoselagua.model.Neighborhood;
import net.jakare.jakarecuidemoselagua.ui.fragments.ItemNeighborhoodFragment;
import net.jakare.jakarecuidemoselagua.ui.template.ExpandAndCollapseCardview;
import net.jakare.pilar.controller.database.SharedPreferencesController;
import net.jakare.pilar.controller.server.GetData;
import net.jakare.pilar.model.response.GenericServerResult;
import net.jakare.pilar.model.response.Rationing;
import net.jakare.pilar.model.response.SubItems;
import net.jakare.pilar.model.response.Zone;

import java.util.Date;
import java.util.List;

import static android.R.attr.key;

/**
 * Created by hola on 02/01/2017.
 */

public class InformationActivity extends AppCompatActivity {
    private ViewGroup linearLayoutDetails;
    private ImageView icon;
    private static final int DURATION = 250;

    private int id;
    private String nomZone;
    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;

    private SharedPreferencesController mPrefs;
    private Context mContext;
    private String urlWebService;

    private TextView dayCurrent;
    private TextView dayVisit;
    private TextView dayVisitF;
    private TextView dayVisitFF;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_information);

        Intent intent = getIntent();
        id = Integer.parseInt(intent.getStringExtra("id"));
        nomZone=intent.getStringExtra("zona");

        dayCurrent=(TextView) findViewById(R.id.dayCurrent);
        dayVisit=(TextView) findViewById(R.id.dayVisit);
        dayVisitF=(TextView) findViewById(R.id.dayVisitFollowing);
        dayVisitFF=(TextView) findViewById(R.id.dayVisitFollowingF);

        setTitle(nomZone);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        loadFragment();

        urlWebService = "http://54.159.229.137/desarrollo/public/json/cuidaragua.json";
        mContext = this;
        mPrefs=new SharedPreferencesController(mContext);
        updateFromWs();

        linearLayoutDetails = (ViewGroup) findViewById(R.id.linearLayoutDetails);
        icon = (ImageView) findViewById(R.id.imageViewExpand);

    }
    public void loadFragment(){
        fragmentManager = getSupportFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        ItemNeighborhoodFragment neighborhood = new ItemNeighborhoodFragment();
        Bundle bundle=new Bundle();
        bundle.putString("id", String.valueOf(id));
        neighborhood.setArguments(bundle);
        fragmentTransaction.replace(R.id.linearLayoutDetails,neighborhood);
        fragmentTransaction.commit();
    }

    public void toggleDetails(View view) {
        if (linearLayoutDetails.getVisibility() == View.GONE) {
            ExpandAndCollapseCardview.expand(linearLayoutDetails, DURATION);
            icon.setImageResource(R.drawable.ic_expand_more);
            rotate(-180.0f);
        } else {
            ExpandAndCollapseCardview.collapse(linearLayoutDetails, DURATION);
            icon.setImageResource(R.drawable.ic_expand_less);
            rotate(180.0f);
        }
    }

    private void rotate(float angle) {
        Animation animation = new RotateAnimation(0.0f, angle, Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f);
        animation.setFillAfter(true);
        animation.setDuration(DURATION);
        icon.startAnimation(animation);
    }
    private void updateFromWs() {
        updateZone();
    }

    private void updateZone(){
        GetData getData=new GetData(new GetData.Response() {
            @Override
            public void onSuccess(GenericServerResult genericServerResult) {
                List<Zone> lstZone=genericServerResult
                        .getResultado().getZonas();
                if(!lstZone.isEmpty()){
                    List<SubItems> lstSubItems=lstZone.get(id-1).getListaSubItems();
                    List<Rationing> lstRoationing=lstSubItems.get(0).getRacionamiento();
                    for (int i=0;i<lstRoationing.size();i++){
                       Toast.makeText(getBaseContext(),lstRoationing.get(i).getFecha(),Toast.LENGTH_SHORT).show();
                        if(i==0){
                            dayCurrent.setText(lstRoationing.get(i).getFecha());
                            dayVisit.setText(lstRoationing.get(i).getFecha());
                        }else if(i==1){
                            dayVisitF.setText(lstRoationing.get(i).getFecha());
                            dayVisitFF.setText(lstRoationing.get(i).getFecha());
                        }
                    }
                    //listaBarrio.add(new Neighborhood());
                    mPrefs.saveDataZone(lstZone);

                }
            }

            @Override
            public void onError(String errorMessage) {
                Toast.makeText(mContext,errorMessage,Toast.LENGTH_SHORT).show();
            }
        });
        getData.execute(urlWebService);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

}
