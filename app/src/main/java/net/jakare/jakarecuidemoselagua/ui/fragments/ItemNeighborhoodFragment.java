package net.jakare.jakarecuidemoselagua.ui.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.gson.Gson;

import net.jakare.jakarecuidemoselagua.R;
import net.jakare.jakarecuidemoselagua.model.Neighborhood;
import net.jakare.jakarecuidemoselagua.model.Zone;
import net.jakare.jakarecuidemoselagua.ui.adapter.NeighborhoodAdapter;
import net.jakare.pilar.controller.database.SharedPreferencesController;
import net.jakare.pilar.controller.server.GetData;
import net.jakare.pilar.model.response.GenericServerResult;
import net.jakare.pilar.model.response.Rationing;
import net.jakare.pilar.model.response.SubItems;

import java.util.ArrayList;
import java.util.List;

import static android.R.attr.key;

/**
 * Created by hola on 03/01/2017.
 */

public class ItemNeighborhoodFragment extends Fragment {

    private RecyclerView recycler;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager lManager;

    List<Neighborhood> listaBarrio = new ArrayList<>();

    private SharedPreferencesController mPrefs;
    private Context mContext;

    private String urlWebService;

    private int id;

    View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view =  inflater.inflate(R.layout.recycleview, container, false);

        Bundle mBundle = new Bundle();
        mBundle = getArguments();
        id=Integer.parseInt(mBundle.getString("id"));

        urlWebService = "http://54.159.229.137/desarrollo/public/json/cuidaragua.json";
        mContext = getActivity();
        mPrefs=new SharedPreferencesController(mContext);
        updateFromWs();

        return view;
    }

    public void usarRecycleView(){
        recycler = (RecyclerView) view.findViewById(R.id.my_recycler_view);
        recycler.setHasFixedSize(true);

        // Usar un administrador para LinearLayout
        lManager = new LinearLayoutManager(getActivity());
        recycler.setLayoutManager(lManager);

        // Crear un nuevo adaptador
        adapter = new NeighborhoodAdapter(listaBarrio);
        recycler.setAdapter(adapter);
        recycler.setLayoutManager(new LinearLayoutManager(getContext()));
    }

    private void updateFromWs() {
        updateZone();
    }

    private void updateZone(){
        GetData getData=new GetData(new GetData.Response() {
            @Override
            public void onSuccess(GenericServerResult genericServerResult) {
                List<net.jakare.pilar.model.response.Zone> lstZone=genericServerResult
                        .getResultado().getZonas();
                if(!lstZone.isEmpty()){
                        List<SubItems> lstSubItems=lstZone.get(id-1).getListaSubItems();
                        for(int j=0;j<lstSubItems.size();j++){
                            listaBarrio.add(new Neighborhood(lstSubItems.get(j).getRacionamiento()
                                    .get(0).getHoraInicio()+"-"+lstSubItems.get(j)
                                    .getRacionamiento().get(0).getHoraFin(),lstSubItems.get(j)
                                    .getNombre()));
                        }
                        //listaBarrio.add(new Neighborhood());
                        mPrefs.saveDataZone(lstZone);
                    usarRecycleView();

                }
            }

            @Override
            public void onError(String errorMessage) {
                Toast.makeText(mContext,errorMessage,Toast.LENGTH_SHORT).show();
            }
        });
        getData.execute(urlWebService);
    }
}
