package net.jakare.jakarecuidemoselagua.ui.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import net.jakare.jakarecuidemoselagua.R;
import net.jakare.jakarecuidemoselagua.ui.activities.InformationActivity;
import net.jakare.jakarecuidemoselagua.model.Zone;

import java.util.List;

/**
 * Created by Ivan on 12/8/2016.
 */
public class ZoneAdapter extends RecyclerView.Adapter<ZoneAdapter.ZoneViewHolder> {
    private static List<Zone> items;
    private View v;

    public class ZoneViewHolder extends RecyclerView.ViewHolder {
        // Campos respectivos de un item
        public TextView nameZone;
        public ZoneViewHolder(final View v) {
            super(v);
            nameZone = (TextView) v.findViewById(R.id.txt_zona);
            nameZone.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //Toast.makeText(v.getContext(),items.get(getLayoutPosition()).getId(),Toast.LENGTH_SHORT).show();

                    Context context = v.getContext();
                    Intent intent = new Intent(context, InformationActivity.class);
                    intent.putExtra("id",items.get(getLayoutPosition()).getId());
                    intent.putExtra("zona",items.get(getLayoutPosition()).getNameZone());
                    context.startActivity(intent);
                }
            });
        }
    }

    public ZoneAdapter(List<Zone> items) {
        this.items = items;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public ZoneViewHolder onCreateViewHolder(final ViewGroup viewGroup, final int i) {
        v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.fragment_item_zone, viewGroup, false);
        /*v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast toast1 =
                        Toast.makeText(v.getContext(),String.valueOf(1), Toast.LENGTH_SHORT);

                toast1.show();
            }
        });*/
        return new ZoneViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ZoneViewHolder viewHolder, int i) {
        viewHolder.nameZone.setText(items.get(i).getNameZone());
    }
}