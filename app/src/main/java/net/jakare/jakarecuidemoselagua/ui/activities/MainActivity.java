package net.jakare.jakarecuidemoselagua.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import net.jakare.jakarecuidemoselagua.R;
import net.jakare.pilar.controller.database.SharedPreferencesController;
import net.jakare.pilar.controller.server.GetData;
import net.jakare.pilar.model.response.GenericServerResult;
import net.jakare.pilar.model.response.Information;
import net.jakare.pilar.model.response.SubItems;
import net.jakare.pilar.model.response.Zone;

import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private SharedPreferencesController mPrefs;
    private Context mContext;

    private TextView mTextViewLogs;
    private Button mButtonUpdate;
    private Button mButtonRead;
    private String urlWebService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        urlWebService = "http://54.159.229.137/desarrollo/public/json/cuidaragua.json";

        mContext = this;
        mPrefs=new SharedPreferencesController(mContext);

        initViews();

        mButtonUpdate.setOnClickListener(this);
        mButtonRead.setOnClickListener(this);
    }

    private void initViews() {
        mTextViewLogs = (TextView) findViewById(R.id.textview_log);
        mButtonUpdate = (Button) findViewById(R.id.button_update);
        mButtonRead=(Button)findViewById(R.id.button_read);
    }

    private void updateFromWs() {
        updateInformation();
    }

    private void updateZone(){
        GetData getData=new GetData(new GetData.Response() {
            @Override
            public void onSuccess(GenericServerResult genericServerResult) {
                List<Zone> lstZone=genericServerResult.getResultado().getZonas();
                if(!lstZone.isEmpty()){
                    List<SubItems> lstSubItems=lstZone.get(0).getListaSubItems();
                    String t="";
                    for(int i=0;i<lstSubItems.size();i++){
                        t=t+lstSubItems.get(i).getNombre();

                    }
                    mTextViewLogs.setText(t);

                    mPrefs.saveDataZone(lstZone);

                }
            }

            @Override
            public void onError(String errorMessage) {
                Toast.makeText(mContext,errorMessage,Toast.LENGTH_SHORT).show();
            }
        });
        getData.execute(urlWebService);
    }
    private void updateInformation(){
        GetData getData=new GetData(new GetData.Response() {
            @Override
            public void onSuccess(GenericServerResult genericServerResult) {
                List<Information> lstInformation=genericServerResult.getResultado().getInformacion();
                if(!lstInformation.isEmpty()){
                    String enElTexto=new Gson().toJson(lstInformation.get(0));
                    mTextViewLogs.setText(enElTexto);
                    mPrefs.saveDataInformation(lstInformation);
                }
            }

            @Override
            public void onError(String errorMessage) {
                Toast.makeText(mContext,errorMessage,Toast.LENGTH_SHORT).show();
            }
        });
        getData.execute(urlWebService);
    }


    /*private void updateFromPrefs() {
        List<Zone> lstZonesGuardadas=mPrefs.getZone();
        if(!lstZonesGuardadas.isEmpty()){
            String enElTexto=new Gson().toJson(lstZonesGuardadas.get(0).getNombre());
            mTextViewLogs.setText(enElTexto);
        } else {
            Toast.makeText(mContext,"No hay datos guardados",Toast.LENGTH_SHORT).show();
        }
    }*/

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button_update:
                updateFromWs();
                break;
            case R.id.button_read:
                //updateFromPrefs();


                Intent ListSong = new Intent(getApplicationContext(), MenuActivity.class);
                startActivity(ListSong);

                break;
            default:
                break;
        }
    }
}
