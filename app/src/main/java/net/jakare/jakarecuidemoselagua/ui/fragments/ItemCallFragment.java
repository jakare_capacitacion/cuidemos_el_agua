package net.jakare.jakarecuidemoselagua.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.gson.Gson;

import net.jakare.jakarecuidemoselagua.R;
import net.jakare.jakarecuidemoselagua.model.Call;
import net.jakare.jakarecuidemoselagua.ui.adapter.CallAdapter;
import net.jakare.pilar.controller.database.SharedPreferencesController;
import net.jakare.pilar.controller.server.GetData;
import net.jakare.pilar.model.response.GenericServerResult;
import net.jakare.pilar.model.response.Information;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hola on 04/01/2017.
 */

public class ItemCallFragment extends Fragment {

    private RecyclerView recycler;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager lManager;

    List<Call> listCall = new ArrayList<>();

    private SharedPreferencesController mPrefs;
    private Context mContext;

    private String urlWebService;
    View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view =  inflater.inflate(R.layout.recycleview, container, false);

        urlWebService = "http://54.159.229.137/desarrollo/public/json/cuidaragua.json";
        mContext = getActivity();
        mPrefs=new SharedPreferencesController(mContext);
        updateFromWs();

        return view;
    }
    private void updateFromWs() {
        updateInformation();
    }

    public void usarRecycleView(){
        recycler = (RecyclerView) view.findViewById(R.id.my_recycler_view);
        recycler.setHasFixedSize(true);

        // Usar un administrador para LinearLayout
        lManager = new LinearLayoutManager(getActivity());
        recycler.setLayoutManager(lManager);

        // Crear un nuevo adaptador
        adapter = new CallAdapter(listCall);
        recycler.setAdapter(adapter);
        recycler.setLayoutManager(new LinearLayoutManager(getContext()));
    }
    private void updateInformation(){
        GetData getData=new GetData(new GetData.Response() {
            @Override
            public void onSuccess(GenericServerResult genericServerResult) {
                List<Information> lstInformation=genericServerResult.getResultado().getInformacion();
                if(!lstInformation.isEmpty()){
                    for (int i=0;i<lstInformation.size();i++){
                        //Toast.makeText(getContext(),enElTexto,Toast.LENGTH_SHORT).show();
                        listCall.add(new Call(lstInformation.get(i).getTipo(),lstInformation.get(i).getValor()));
                      // listCall.add(new Call(enElTexto.toString(),enElTexto2.toString()));
                        mPrefs.saveDataInformation(lstInformation);
                    }
                    usarRecycleView();
                }
            }

            @Override
            public void onError(String errorMessage) {
                Toast.makeText(mContext,errorMessage,Toast.LENGTH_SHORT).show();
            }
        });
        getData.execute(urlWebService);
    }
}
