package net.jakare.jakarecuidemoselagua.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import net.jakare.jakarecuidemoselagua.R;
import net.jakare.jakarecuidemoselagua.model.Neighborhood;

import java.util.List;

/**
 * Created by hola on 03/01/2017.
 */

public class NeighborhoodAdapter extends RecyclerView.Adapter<NeighborhoodAdapter.NeighborhoodViewHolder> {
    private static List<Neighborhood> items;
    private View v;

    public class NeighborhoodViewHolder extends RecyclerView.ViewHolder {
        // Campos respectivos de un item
        public TextView NameBarrio;
        public TextView hour;
        public NeighborhoodViewHolder(final View v) {
            super(v);
            NameBarrio = (TextView) v.findViewById(R.id.txtName);
            hour =(TextView)v.findViewById(R.id.txtHour);
            /*nombreCharlaHorario.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int pos = getAdapterPosition();
                    Zona horario = items.get(pos);
                    String key = horario.getKey();

                    Context context = v.getContext();
                    Intent intent = new Intent(context, InformacionActivity.class);
                    intent.putExtra("key",key);
                    context.startActivity(intent);
                }
            });*/
        }
    }

    public NeighborhoodAdapter(List<Neighborhood> items) {
        this.items = items;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public NeighborhoodViewHolder onCreateViewHolder(final ViewGroup viewGroup, final int i) {
        v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.fragment_item_neighborhood, viewGroup, false);
        /*v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast toast1 =
                        Toast.makeText(v.getContext(),String.valueOf(1), Toast.LENGTH_SHORT);

                toast1.show();
            }
        });*/
        return new NeighborhoodViewHolder(v);
    }

    @Override
    public void onBindViewHolder(NeighborhoodViewHolder viewHolder, int i) {
        viewHolder.NameBarrio.setText(items.get(i).getNameNeighborhood());
        viewHolder.hour.setText(items.get(i).getHour());
    }
}
